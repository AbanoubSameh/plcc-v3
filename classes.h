/*
 * Copyright (C) 2019  Abanoub Sameh
 *
 * This file is part of plcc-v3.
 *
 * plcc-v3 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * plcc-v3 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with plcc-v3.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef _classes_h_
#define _classes_h_

#include <inttypes.h>

#define CHR     225
#define VDU     224
#define VDB     223
#define VST     222

typedef struct Operand {
    uint8_t type;
    void *op;
} Operand;

typedef struct Uni_gate {
    uint8_t type;
    Operand *operand1;
} Uni_gate;

typedef struct Bin_gate {
    uint8_t type;
    Operand *operand1;
    Operand *operand2;
} Bin_gate;

typedef struct Inst {
    Operand *current;
    struct Inst *next;
} Inst;

typedef struct Stmt {
    uint8_t type;
    Operand *cond;
    Inst *body;
} Stmt;

// this should be put in a Operand file
Operand* new_Operand(void*, uint8_t);
void delete_Operand(Operand*);

// this should be put in a Uni_gate file
Uni_gate* new_Uni_gate(uint8_t, Operand*);
void delete_Uni_gate(Uni_gate*);

// this should be put in a Bin_gate file
Bin_gate* new_Bin_gate(uint8_t, Operand*, Operand*);
void delete_Bin_gate(Bin_gate*);

// this should be put in a Inst file
Inst* new_Inst(Operand*);
void set_inst_next(Inst*, Inst*);
void delete_Inst(Inst*);

// this should by put in a Stmt file
Stmt *new_stmt(Operand*, Inst*, uint8_t);
void delete_Stmt(Stmt*);

#endif

