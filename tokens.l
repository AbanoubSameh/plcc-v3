/*
 * Copyright (C) 2019  Abanoub Sameh
 *
 * This file is part of plcc-v3.
 *
 * plcc-v3 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * plcc-v3 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with plcc-v3.  If not, see <https://www.gnu.org/licenses/>.
 */

%{
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "parser.h"
void yyerror(char *s);

%}

%option noyywrap

%%

        /* keywords */
"loop"      {return LOOP;}
"int"       {return INT;}
"double"    {return DOUBLE;}
"if"        {return IF;}
"else"      {return ELSE;}
"void"      {return VOID;}

        /* symbols */
\(          {return ORB;}
\)          {return ORB;}

\[          {return OSB;}
\]          {return CSB;}

\{          {return OWB;}
\}          {return OWB;}
\;`         {return SMC;}

        /* PSOP */
\-\>        {return REF;}

\+\+        {return INC;}
\-\-        {return DEC;}

        /* UNOP */
\!          {return NOT;}
\~          {return LNT;}
\<\|        {return RED;}
\>\|        {return FED;}

        /* MLOP */
\*          {return MUL;}
\/          {return DIV;}
\%          {return MOD;}

        /* ADOP */
\+          {return ADD;}
\-          {return SUB;}

        /* SHOP */
\<\<        {return SHL;}
\<\<\<      {return ROL;}
\>\>        {return SHR;}
\>\>\>      {return ROR;}

        /* REOP */
\<          {return SMT;}
\>          {return GRT;}
\<\=        {return STE;}
\>\=        {return GTE;}

        /* EQOP */
\=\=        {return ISE;}
\!\=        {return NOE;}

       /* some other stuff */
\&          {return LAD;}

\^          {return XOR;}

\|          {return LOR;}

\&\&        {return AND;}

\|\|        {return ORR;}

        /* ASOP */
\=          {return EQU;}

\+\=        {return PEQ;}
\-\=        {return SEQ;}
\*\=        {return TEQ;}
\/\=        {return DEQ;}
\%\=        {return MEQ;}

\>\>\=      {return SHREQ;}
\<\<\=      {return SHLEQ;}
\>\>\>\=    {return ROREQ;}
\<\<\<\=    {return ROLEQ;}

\&\=        {return LADEQ;}
\|\=        {return LOREQ;}
\^\=        {return LEXEQ;}


                    /* identifiers */
[a-zA-Z][a-zA-Z0-9._$]*         {yylval.str = yytext; return STR;}
[1-9][0-9]*                     {yylval.str = yytext; return NUM;}
[1-9][0-9]*\.[0-9]*             {yylval.str = yytext; return DBL;}
[ \t]+                          /* ignore whitespace */
\n                              /* ignore newline */
[1-9][0-9]*\.[0-9]*\.[0-9]*     {yyerror("wrong token\n");}

%%
