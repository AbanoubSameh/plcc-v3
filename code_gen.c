/*
 * Copyright (C) 2019  Abanoub Sameh
 *
 * This file is part of plcc-v3.
 *
 * plcc-v3 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * plcc-v3 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with plcc-v3.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdbool.h>
#include <inttypes.h>

#include "code_gen.h"
#include "definitions.h"

void gen_Uni_gate(Uni_gate *root) {
    gen_op(root->operand1);
}

void gen_Bin_gate(Bin_gate *root) {
    gen_op(root->operand1);
    gen_op(root->operand2);
    gen_oper(root->type);
}

void gen_stmt(Stmt* root) {
    printf("statement:\n");
    printf("\tcondition:\n");
    gen_op(root->cond);
    printf("\tbody:");
    gen_Inst(root->body);
    printf("end of statement\n");
}

void gen_op(Operand *root) {
    //printf("came here\n");
    if (root->type == CHR) {
        printf("%s ", (char*) root->op);
    } else if (root->type == VDU) {
        //printf("Ugate\n");
        gen_Uni_gate((Uni_gate*) root->op);
    } else if (root->type == VDB) {
        //printf("Bgate\n");
        gen_Bin_gate((Bin_gate*) root->op);
    } else if (root->type == VST) {
        gen_stmt((Stmt*) root->op);
    } else {
        printf("wrong operand type\n");
    }
}

void gen_Inst(Inst *toGen) {
    if (toGen == NULL) {
        return;
    }
    gen_op(toGen->current);
    printf("\n");
    gen_Inst(toGen->next);
    delete_Inst(toGen);
}

void gen_oper(int8_t type) {
    if (type == PLUS) {
        printf("ADD ");
    } else if (type == MINUS) {
        printf("SUB ");
    } else if (type == TIMES) {
        printf("MUL ");
    } else if (type == DIVIDE) {
        printf("DIV ");
    } else if (type == MODOLO) {
        printf("MOD ");
    } else {
        printf("\n*** wrong operation ***\n");
    }
}

