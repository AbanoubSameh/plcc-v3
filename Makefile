CC := gcc
CFLAGS := -std=gnu99 -g# -Wall -Wextra -Werror

ODIR := obj
SRC := $(wildcard *.c)
OBJ := $(SRC:%.c=${ODIR}/%.o)
HDR := $(wildcard *.h)

all: $(ODIR)/parser.o $(ODIR)/lexer.o $(OBJ) out

$(ODIR)/%.o : %.c $(HDR)
	$(CC) $(CFLAGS) -c $< -o $@

$(ODIR)/parser.o: rules.y
	yacc -o parser.c -d rules.y
	$(CC) $(CFLAGS) -c parser.c -o $(ODIR)/parser.o

$(ODIR)/lexer.o: tokens.l parser.h
	lex -o lexer.c tokens.l
	$(CC) $(CFLAGS) -c lexer.c -o $(ODIR)/lexer.o

out:
	$(CC) $(CFLAGS) $(OBJ) $(ODIR)/parser.o $(ODIR)/lexer.o -o out

clean:
	rm -f ${ODIR}/*

cleanall: clean
	rm -f out parser.c parser.h lexer.c

rebuild: cleanall
	$(MAKE) all

