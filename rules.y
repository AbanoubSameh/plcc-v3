/*
 * Copyright (C) 2019  Abanoub Sameh
 *
 * This file is part of plcc-v3.
 *
 * plcc-v3 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * plcc-v3 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with plcc-v3.  If not, see <https://www.gnu.org/licenses/>.
 */

%{
void yyerror(char *s);
int yylex();
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "classes.h"
#include "definitions.h"

Inst* inst;

%}

// keywords
%token LOOP INT DOUBLE IF ELSE VOID

// symbols
%token ORB CRB OSB CSB OWB CWB SMC
%token REF
%token INC DEC
%token NOT LNT RED FED
%token MUL DIV MOD
%token ADD SUB
%token SHL ROL SHR ROR
%token SMT GRT STE GTE
%token ISE NOE
%token LAD
%token XOR
%token LOR
%token AND
%token ORR
%token EQU PEQ SEQ TEQ DEQ MEQ
%token SHREQ SHLEQ ROREQ ROLEQ LADEQ LOREQ LEXEQ

// identifiers
%token STR NUM DBL

%start INST

%union {
    char* str;
    int num;
    struct Operand *op;
    struct Inst *inst;
}

%type <str> STR NUM DBL
%type <op> ADDOP MULOP //OP
%type <num> MLOPER ADOPER //PSOPER UNOPER SHOPER REOPER EQOPER ASOPER

%%

//PSOPER : INC DEC REF;
//UNOPER : NOT LNT SUB INC DEC MUL LAD;

MLOPER
    : MUL   {$$ = TIMES;}
    | DIV   {$$ = DIVIDE;}
    | MOD   {$$ = MODOLO;}
    ;

ADOPER
    : ADD   {$$ = PLUS;}
    | SUB   {$$ = MINUS;}
    ;

//SHOPER : SHL ROL SHR ROR;
//REOPER : SMT GRT GTE STE;
//EQOPER : ISE | NOE;
//ASOPER : EQU | PEQ | SEQ | TEQ | DEQ | MEQ
//     | SHREQ | SHLEQ | ROREQ | ROLEQ | LADEQ | LOREQ | LEXEQ;

INST
    : ADDOP SMC                 {inst = new_Inst($1); return 1;}
    ;

//OP : ADDOP                      {$$ = $1;};

ADDOP
    : ADDOP ADOPER MULOP        {printf("ADD\n"); $$ = new_Operand(new_Bin_gate($2,
    (void*) $1, (void*) $3), VDB);}
    | MULOP
    ;

MULOP
    : MULOP MLOPER NUM          {printf("MUL: %s\n", yylval.str); $$ = new_Operand(new_Bin_gate($2,
    (void*) $1, (void*) new_Operand(yylval.str, CHR)), VDB);}
    | NUM                       {printf("NUM: %s\n", yylval.str); $$ = new_Operand(yylval.str, CHR);}
    ;

%%

void yyerror(char *s) {
    printf("%s\n", s);
}
