/*
 * Copyright (C) 2019  Abanoub Sameh
 *
 * This file is part of plcc-v3.
 *
 * plcc-v3 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * plcc-v3 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with plcc-v3.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <inttypes.h>

#include "classes.h"

#ifdef PGENALL
#define PGENOP
#define PGENUN
#define PGENBIN
#endif

char* strduplic(char* str) {
    char *new_str = malloc(strlen(str) + 1);
    if (new_str == NULL) {
        return NULL;
    }
    strcpy(new_str, str);
    return new_str;
}

// operand
Operand* new_Operand(void *op, uint8_t type) {
    Operand *operand = malloc(sizeof(Operand));
    operand->type = type;
    if (type == CHR) {
#   ifdef PGENOP
        printf("op created at: %ld with value: %s\n", (long) operand, (char*) op);
#   endif
    operand->op = (void*) strduplic(op);
    } else if (type == VDU || type == VDB) {
#   ifdef PGENOP
        printf("op created at: %ld with value: %ld\n", (long) operand, (long) op);
#   endif
    operand->op = op;
    }
    return operand;
}

void delete_Operand(Operand *operand) {
    if (operand->type == CHR) {
        free(operand->op);
    } else if (operand->type == VDU) {
        delete_Uni_gate(operand->op);
    } else if (operand->type == VDB) {
        delete_Bin_gate(operand->op);
    } else {
        printf("wrong type, could not delete operand\n");
    }
    free(operand);
}

// Uni_gate
Uni_gate* new_Uni_gate(uint8_t type, Operand *operand) {
    Uni_gate *gate = malloc(sizeof(Uni_gate));
    gate->type = type;
    gate->operand1 = operand;
#ifdef PGENUN
    printf("gate created at: %ld\n", (long) gate);
#endif
    return gate;
}

void delete_Uni_gate(Uni_gate *gate) {
    delete_Operand(gate->operand1);
    free(gate);
}

// Bin_gate
Bin_gate* new_Bin_gate(uint8_t type, Operand *operand1, Operand *operand2) {
    Bin_gate *gate = malloc(sizeof(Bin_gate));
    gate->type = type;
    gate->operand1 = operand1;
    gate->operand2 = operand2;
#ifdef PGENBIN
    printf("gate created at: %ld\n", (long) gate);
#endif
    return gate;
}

void delete_Bin_gate(Bin_gate *gate) {
    delete_Operand(gate->operand1);
    delete_Operand(gate->operand2);
    free(gate);
}

// Inst
Inst* new_Inst(Operand *current) {
    Inst *inst = malloc(sizeof(Inst));
    inst->current = current;
    inst->next = NULL;
#ifdef PGENINST
    printf("a new instruction was created at %ld\n", inst);
#endif
    return inst;
}

void set_inst_next(Inst *inst, Inst *next) {
    inst->next = next;
#ifdef PGENINST
    printf("inst %ld set as next for inst %ld\n", next, inst);
#endif
}

void delete_Inst(Inst *toDel) {
    delete_Operand(toDel->current);
    free(toDel);
}

// Stmt
Stmt* new_stmt(Operand *cond, Inst *body, uint8_t type) {
    Stmt *stmt = malloc(sizeof(Stmt));
    stmt->cond = cond;
    stmt->body = body;
    stmt->type = type;
    printf("new statement created with type: %d\nand condition: %ld\nand body: %ld\n", type, cond, body);
    return stmt;
}

void delete_Stmt(Stmt *toDel) {
    delete_Operand(toDel->cond);
    delete_Inst(toDel->body);
    free(toDel);
}

